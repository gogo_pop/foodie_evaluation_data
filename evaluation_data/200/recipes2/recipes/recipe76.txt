Combine the spinach, strawberries, Gorgonzola cheese, and pecans in a large bowl.    
Stir the balsamic vinegar and honey together in a bowl; slowly stream the olive oil into the mixture while whisking continuously. Season with salt and pepper. Drizzle the dressing over the salad just before serving.    

