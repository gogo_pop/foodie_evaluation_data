Preheat oven to 425 degrees F (220 degrees C). Line a baking sheet with aluminum foil and spray with cooking spray.
Stir bread crumbs and Parmesan cheese together in a shallow bowl. Whisk eggs in a separate shallow bowl. 
Working in batches, dip zucchini strips into egg mixture, shake to remove any excess, and roll strips in bread crumb mixture to coat. Transfer coated zucchini strips to the prepared baking sheet. 
Bake zucchini fries in the preheated oven, turning once, until golden and crisp, 20 to 24 minutes. 

