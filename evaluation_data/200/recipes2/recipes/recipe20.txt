In a large pot with boiling salted water cook pasta until al dente. Drain.    
Meanwhile, in a large bowl whisk together the olive oil, red wine vinegar, and dried basil. Add salt and pepper to taste. Stir in the tomatoes, corn kernels, and scallions. Let sit for 5 to 10 minutes.    
Toss pasta with tomato mixture. Sprinkle with grated parmesan cheese. Garnish with fresh basil, if desired.    

