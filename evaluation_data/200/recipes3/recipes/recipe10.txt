Preheat oven to 500 degrees.
Coat carrots with oil and salt. Place on a baking sheet and roast for 15 minutes until well charred and easily pierced by a knife.
While carrots are roasting, make pesto by placing all ingredients except olive oil into a small mini chopper or blender. Grind on medium speed until coarse, then slowly pour in olive oil to emulsify into grainy pesto. Adjust seasoning to taste.
When carrots are cooked, remove from oven and cut in half lengthwise and crosswise. Dress with pesto to serve.