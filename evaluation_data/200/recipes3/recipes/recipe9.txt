You will need a 20cm loose-bottom baking tin.
Preheat the oven to 160 C (140 C with fan) Gas mark 3. Line the baking tin with greaseproof paper.
Pour the melted butter over the biscuits and mix well. Spoon the mixture into the base of the baking tin. Chill for 10 minutes.
In a large bowl soften the soft cheese, then add the Carnation Chocolate filling and topping and the 3 large eggs, mix well. Pour over the biscuit base.
Place in the centre of the oven and bake for 45 minutes until firm. Remove from the oven and leave to cool before chilling for 4 hours.
Just before serving tip the fresh raspberries into a blender to make a coulis. Spread the raspberry coulis over the cheesecake.