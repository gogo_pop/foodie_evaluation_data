Add the beef broth and Worcestershire sauce to a 6-quart slow cooker.

Add the pork, turkey, breadcrumbs, eggs, allspice, onion powder, 2 teaspoons salt and 1/2 teaspoon pepper to a large bowl. Mix the meat with your hands until the mixture is uniform in color and texture. Scoop level tablespoonfuls, and roll them into balls; place them in a single layer in the slow cooker. When the bottom of the cooker is covered, add a second layer, starting on the outer edge. (The outside of the cooker is hotter; meatballs on the edge will cook more quickly than those in the center.) Cover and cook on high for 2 hours, until the meatballs are cooked through.

Meanwhile, rub the butter and flour together in a small bowl to make a smooth paste (which will be used to thicken the sauce). Pinch off 1/2-inch balls, and set on a small plate.

Transfer the meatballs with a slotted spoon from the slow cooker to a large bowl, leaving the cooking liquid in the pot. (You should have about 1 1/2 cups liquid.) When removing the meatballs, start from the outer edges and scoop carefully. Some may be stuck together; gentle coaxing with the spoon will separate them.

Whisk the butter-flour paste into the cooking liquid, 1 or 2 balls at a time, to incorporate. Whisk in the sour cream and jelly, if using. Return the meatballs to the slow cooker, and stir with the spoon to coat. Cook on high, uncovered, until the sauce is thickened, 30 minutes. Adjust the setting to keep warm, sprinkle the meatballs with the parsley and serve.

