Steam or boil the Tenderstem for 3-4 minutes until tender.
Meanwhile, mix together the yoghurt, coriander, mint, lime juice and lime zest and leave to infuse for a few minutes.
Put the chicken, rice, cooked Tenderstem, almonds and spring onions in a large mixing bowl and season. Add the yoghurt dressing and combine the ingredients together.
Serve with a sprinkling of chopped coriander.