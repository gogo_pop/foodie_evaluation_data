In a bowl, mix the tomatoes, onion, and basil. Season with salt and pepper, and drizzle with balsamic vinegar and olive oil. Spoon over the fried chicken to serve.    

