Stir white wine, lime juice, water, olive oil, thyme, cilantro, black pepper, marjoram, and dill together in a bowl; pour into a large sealable plastic bag; add pork loin, squeeze air from the bag, and seal bag. Assure pork loin is completely covered in marinade and put bag in a bowl.    
Marinate pork in refrigerator, 8 hours to overnight.    
Heat a skillet over medium-high heat.    
Remove pork loin from marinade, shake to remove excess liquid but allow any herbs sticking to meat to remain.    
Sear pork loin in hot skillet until browned completely, 2 to 3 minutes per side; transfer to the crock of a slow cooker.    
Pour chicken broth over the pork loin. Arrange potatoes, carrots, and onion around the meat in the slow cooker crock.    
Cook on High until the meat is tender, at least 4 hours.    

