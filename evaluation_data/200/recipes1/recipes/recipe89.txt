Preheat oven to 400 degrees F (200 degrees C).    
Spread crushed onions in a shallow bowl. Pour beaten egg into a separate shallow bowl.    
Dip chicken into beaten egg, then press into crushed onions. Gently tap chicken to let loose pieces fall away. Put breaded chicken to a baking sheet.    
Bake chicken until no longer pink in the center and the juices run clear, about 20 minutes. An instant-read thermometer inserted into the center should read at least 165 degrees F (74 degrees C).    

