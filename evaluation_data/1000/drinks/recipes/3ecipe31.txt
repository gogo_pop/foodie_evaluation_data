Place the banana, grapes, yogurt, apple and spinach into a blender. Cover, and blend until smooth, stopping frequently to push down anything stuck to the sides. Pour into glasses and serve.    

