In plastic pitcher empty passion fruit juice and 6 cups water.  Mix well. Toss in the mango chunks. Pour in vodka. Mix again and place in freezer for at least 12 hours    
The vodka will prevent mixture from freezing completely. Mix again after removing from freezer. Serve immediately.    

