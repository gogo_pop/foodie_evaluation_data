Place the mint leaves, lime wedges, and ice into a cocktail shaker; mash well with a cocktail muddler. Pour in the simple syrup, lime juice, rum, cranberry juice, and soda water. Cover, and shake until the outside of the shaker has frosted. Strain into a chilled glass to serve.    

