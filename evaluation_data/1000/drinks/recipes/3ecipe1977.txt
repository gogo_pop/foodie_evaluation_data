Pour graham cracker crumbs onto a small, shallow plate. Moisten the rim of a chilled martini glass and dip the moistened glasses into the graham cracker crumbs.    
Fill a shaker with ice; pour chocolate liqueur, half-and-half, vodka, and Irish cream over ice. Shake until cocktail is combined and strain into prepared martini glass. Thread marshmallows onto a skewer and garnish cocktail with marshmallows.    

