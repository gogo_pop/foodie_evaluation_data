Blend ice cubes, peppermint candies, and chocolate chips together in a blender until evenly blended and slightly grainy. Add ice cream, peppermint extract, and red food coloring; blend until smooth. Add milk and blend until desired consistency is reached.    

