Fill a cocktail shaker with ice. Pour in gin, orange juice, Pernod, and grenadine. Shake well and strain into a chilled glass. Garnish with a twist of orange peel.    

