Pour the tequila and triple sec into a pitcher. Sprinkle in the confectioners' sugar, and stir to dissolve. Add the ice, and pour in the pomegranate juice and lime juice. Stir to mix, then serve. You can add more tequila to taste if you're a professional.    

