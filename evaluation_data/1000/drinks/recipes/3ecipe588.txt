Mix coffee and sugar. Add boiling water and stir until dissolved.  Cool.    
Add Vodka and pour into two 26 ounce empty bottles. Split vanilla into 4 pieces and add to bottles.    
Close bottles tightly and keep in a cool dark place for one month.    

