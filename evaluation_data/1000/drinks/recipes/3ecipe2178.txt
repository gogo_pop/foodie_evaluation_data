In a mixing bowl, combine water, gin, tea, frozen lemonade, frozen orange juice, and sugar. Mix well and store in the freezer (covered) until ready to use.    
To serve: in a tall glass combine frozen mixture to taste with lemon-lime flavored carbonated beverage.    

