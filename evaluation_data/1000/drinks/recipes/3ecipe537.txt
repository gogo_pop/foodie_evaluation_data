Combine Chardonnay wine, pineapple chunks, lemonade concentrate, rum, triple sec, grapes, apple, orange, and lime in a large pitcher. Refrigerate at least 2 hours or overnight.    
Stir lemon-lime soda into pitcher just before serving.    

