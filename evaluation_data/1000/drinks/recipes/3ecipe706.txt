Place egg yolks in a saucepan. Whisk in sugar until well blended and creamy. Add milk and cream; whisk until blended. Place pan over medium heat; whisk frequently until mixture reaches a temperature of between 170 to 180 degrees F (75 to 80 degrees C) using an instant read thermometer. Remove from heat. Stir in nutmeg and whiskey.    
Chill pan in cold water until cool. Transfer mixture to a container (a large pitcher is ideal). Cover and refrigerate until thoroughly chilled, 2 to 3 hours.    
Place egg whites in a bowl and whisk until soft peaks form. Add sugar and continue whisking until egg whites hold a firmer peak. Whisk egg whites into custard base until thoroughly blended. Refrigerate until chilled, about 30 minutes.    
Whisk eggnog again just before serving and between each pour to ensure even distribution of the egg whites which tend to float to the top. Serve with a dusting of nutmeg.    

