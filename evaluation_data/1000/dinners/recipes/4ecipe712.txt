Place the chicken breast halves in a dish with the 1 1/2 cups of teriyaki sauce. Cover and refrigerate 8 hours or overnight.    
Preheat a grill for high heat.    
Lightly oil the grill grate. Place chicken breasts on grill, and discard marinade. Cook for 8 minutes per side, or until juices run clear. Brush with the remaining teriyaki sauce during the last 5 minutes. When almost done, place one pineapple ring on top of each breast, and brush with melted butter.    
In a small saucepan over medium heat, mix the brown sugar, soy sauce, pineapple juice, and Worcestershire sauce. Cook, stirring occasionally, until sugar is dissolved. Serve with chicken for dipping!    

