Rub chicken breasts with olive oil and garlic; sprinkle with salt, black pepper, rosemary, and basil. Arrange chicken in a large baking dish and refrigerate at least 45 minutes.    
Preheat oven to 375 degrees F (190 degrees C).    
Bake in preheated oven until chicken meat is no longer pink at the bone and the juices run clear, 45 to 60 minutes. An instant-read thermometer inserted in the thickest part of the breast meat should read 165 degrees F (75 degrees C).    

