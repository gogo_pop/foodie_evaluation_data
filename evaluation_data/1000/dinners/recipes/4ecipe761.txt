Preheat oven to 350 degrees F (175 degrees C). Spray a baking dish with cooking spray.    
Whisk egg in a shallow bowl.    
Mix Parmesan cheese and Cajun seasoning together on a plate.    
Dip each pork chop into egg. Press into Parmesan mixture until coated on both sides. Place in the prepared baking dish.    
Bake in the preheated oven until golden and an instant-read thermometer inserted into the center reads at least 145 degrees F (63 degrees C), 35 to 40 minutes.    

