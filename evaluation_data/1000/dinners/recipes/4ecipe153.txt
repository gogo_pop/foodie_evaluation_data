Preheat oven to 425 degrees F (220 degrees C). Roll out bottom pie crust, press into a 10 inch pie pan, and set aside.    
Melt 2 tablespoons butter in a large skillet over medium heat; add the onion, celery, carrots, parsley, oregano, and salt and pepper. Cook and stir until the vegetables are soft. Stir in the bouillon and water. Bring mixture to a boil. Stir in the potatoes, and cook until tender but still firm.    
In a medium saucepan, melt the remaining 2 tablespoons butter. Stir in the turkey and flour. Add the milk, and heat through. Stir the turkey mixture into the vegetable mixture, and cook until thickened. Cool slightly, then pour mixture into the unbaked pie shell. Roll out the top crust, and place on top of filling. Flute edges, and make 4 slits in the top crust to let out steam.    
Bake in the preheated oven for 15 minutes. Reduce oven temperature to 350 degrees F (175 degrees C), and continue baking for 20 minutes, or until crust is golden brown.    

