In a medium saucepan, bring the rice and water to a boil. Reduce heat to low, cover, and cook 20 minutes.    
Heat the garlic oil in a skillet over medium heat. Season the salmon with salt, pepper, dill, and paprika, and cook in the hot oil 1 to 2 minutes on each side, until tender enough to break apart. Break salmon into cubes with a spatula or fork. Mix in the tomatoes, garlic, and lemon juice. Continue cooking until salmon is easily flaked with a fork.    
Mix the parsley, Parmesan cheese, butter, and hot pepper sauce into the skillet, and continue cooking 1 to 2 minutes, until well mixed. Serve over the cooked rice.    

