In a small bowl, combine the honey, mustard, rosemary, ground black pepper, lemon zest and garlic. Mix well and apply to the lamb. Cover and marinate in the refrigerator overnight.    
Preheat oven to 450 degrees F (230 degrees C).    
Place lamb on a rack in a roasting pan and sprinkle with salt to taste.    
Bake at 450 degrees F (230 degrees C) for 20 minutes, then reduce heat to 400 degrees F (200 degrees C) and roast for 55 to 60 more minutes for medium rare. The internal temperature should be at least 145 degrees F (63 degrees C) when taken with a meat thermometer. Let the roast rest for about 10 minutes before carving.    

