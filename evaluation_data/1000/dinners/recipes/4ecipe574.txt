Heat oil in a large skillet over medium-high heat. Saute jalapeno pepper and garlic in hot oil until fragrant, about 1 minute.    
Stir black beans, tomatoes, yellow corn, quinoa, and chicken broth into skillet; season with red pepper flakes, chili powder, cumin, salt, and black pepper. Bring to a boil, cover the skillet with a lid, reduce heat to low, and simmer until quinoa is tender and liquid is mostly absorbed, about 20 minutes. Stir avocado, lime juice, and cilantro into quinoa until combined.    

