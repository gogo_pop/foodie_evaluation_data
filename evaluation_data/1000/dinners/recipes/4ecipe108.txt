In a small bowl combine yogurt, cucumber, dill, salt, pepper and mayonnaise and mix well. Chill for at least 30 minutes.    
In a large bowl mash chickpeas until thick and pasty; don't use a blender, as the consistency will be too thin. In a blender, process onion, parsley and garlic until smooth. Stir into mashed chickpeas.    
In a small bowl combine egg, cumin, coriander, salt, pepper, cayenne, lemon juice and baking powder. Stir into chickpea mixture along with olive oil.  Slowly add bread crumbs until mixture is not sticky but will hold together; add more or less bread crumbs, as needed. Form 8 balls and then flatten into patties.    
Heat 1 inch of oil in a large skillet over medium-high heat. Fry patties in hot oil until brown on both sides. Serve two falafels in each pita half topped with chopped tomatoes and cucumber sauce.    

