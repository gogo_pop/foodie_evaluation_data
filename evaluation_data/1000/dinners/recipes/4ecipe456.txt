In a large, deep skillet fry the bacon over medium-high heat until evenly brown.  Drain the bacon fat.  Add the onions to the pan with the bacon and cook 5 minutes or until the onions are translucent.  Stir in the shrimp and chipotle chiles; cook 4 minutes or until heated through.    
Heat tortillas on an ungreased skillet over medium-high heat for 10 to 15 seconds. Turn and heat for another 5 to 10 seconds.  Fill the heated tortillas with shrimp mixture.  Sprinkle with cilantro, lime juice, and salt.    

