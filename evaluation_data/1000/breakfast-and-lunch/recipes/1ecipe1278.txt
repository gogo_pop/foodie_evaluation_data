Arrange bread in the bottom of a lightly greased medium baking dish, and drizzle with butter. Top with Swiss cheese, Monterey Jack cheese, and ham.    
In a medium bowl, blend eggs, milk, and mustard. Season with salt and pepper. Pour into the baking dish. Cover, and refrigerate 8 hours, or overnight.    
Preheat oven to 325 degrees F (165 degrees C). Blend sour cream and Parmesan cheese in a small bowl, and set aside.    
Bake the layered mixture 1 hour in the preheated oven.    
Spread sour cream mixture over the surface of the baked dish, and continue cooking about 10 minutes, until surface is lightly browned. Allow to sit about 15 minutes before serving.    

