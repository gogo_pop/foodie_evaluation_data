Place potatoes in a saucepan and add enough water to cover. Bring to a boil, and cook until just starting to be tender, about 5 minutes. Drain and place under cool running water to cool, then drain and pat dry. Grate into a large bowl.    
Separate the potato shreds into two sections, and place each one on a square of waxed paper. Pat each section into a 6 inch circle.    
Place bacon in a skillet over medium-high heat. Fry until evenly browned, then set aside to drain on paper towels. In a nonstick skillet over low heat, cook eggs until lightly scrambled. Eggs need to stay slightly wet.    
Heat oil in a large skillet over medium heat. While the oil heats, place the egg, bacon strips and cheese in even layers on top of one of the potato circles. Place the other circle on the top, using the waxed paper to pick it up. Place the whole hash-brown sandwich into the skillet, and fry until nicely browned on both sides, about 10 minutes per side. Cut into wedges, and serve hot.    

