Grease and flour two 7x3 inch loaf pans.  Preheat oven to 350 degrees F (175 degrees C).    
In one bowl, whisk together flour, soda, salt, and sugar.  Mix in slightly beaten eggs, melted butter, and mashed bananas.  Stir in nuts if desired.  Pour into prepared pans.    
Bake at 350 degrees F (175 degrees C) for 1 hour, or until a wooden toothpick inserted in the center comes out clean.    

