Preheat oven to 350 degrees F (175 degrees C). Lightly grease a 9x13 inch baking dish.    
In a large skillet or frying pan, heat oil over medium high heat. Add zucchini, mushrooms, onion, green pepper and garlic; saute until tender. Remove from heat and let cool slightly.    
In a large bowl, beat together the eggs and cream. Stir in cream cheese, cheddar cheese, bread cubes and sauteed vegetables. Season with salt and pepper. Mix well and pour into prepared baking dish.    
Bake in preheated oven for one hour, or until center is set. Serve hot or cold.    

