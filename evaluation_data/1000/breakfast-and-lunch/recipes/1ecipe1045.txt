Preheat oven to 350 degrees F (175 degrees C). Grease a 9x5 inch loaf pan.    
In a large bowl, combine the sugar, flour, cinnamon walnuts, apples, and cheese. Add the eggs, melted butter or margarine, and milk; stir until well blended. Pour into prepared loaf pan.    
Bake in preheated oven for 1 hour, or until done.  If loaf starts to brown too much, cover with foil.    

