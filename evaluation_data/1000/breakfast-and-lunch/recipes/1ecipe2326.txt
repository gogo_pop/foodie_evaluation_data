Preheat oven to 325 degrees F (165 degrees C).    
Press pie crust into a 9-inch deep dish pie plate. Cover the edges of the crust with aluminum foil.    
Bake pie crust until lightly browned, about 12 minutes. Set pie crust aside to cool. Increase oven heat to 350 degrees F (175 degrees C).    
Cook bacon in a large skillet over medium-high heat until crisp, about 10 minutes. Drain the bacon slices on paper towels, while retaining drippings in the skillet.    
Stir zucchini, mushrooms, onion, and garlic into the reserved bacon drippings; season with Italian seasoning, dill, parsley, garlic salt, and black pepper. Cook and stir the zucchini mixture until the zucchini is tender yet crisp, 5 to 7 minutes; remove from heat.    
Chop the bacon and stir through the zucchini mixture.    
Sprinkle about half the mozzarella cheese into the bottom of the pie crust. Spoon zucchini mixture over the mozzarella cheese layer. Pour eggs over the zucchini mixture; top with remaining mozzarella cheese. Sprinkle Parmesan cheese over the mozzarella cheese. Put pie plate onto a baking sheet.    
Bake in preheated oven until the top begins to brown, about 30 minutes.    

