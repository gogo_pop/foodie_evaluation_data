Preheat oven to 350 degrees F (175 degrees C). Prepare a 9x13 inch baking dish with the melted butter.    
Whisk together the eggs, pepperjack cheese, cottage cheese, flour, and salt in a large bowl. Pour the mixture into the prepared baking dish.    
Bake in the preheated oven until eggs are completely set, about 1 hour.    

