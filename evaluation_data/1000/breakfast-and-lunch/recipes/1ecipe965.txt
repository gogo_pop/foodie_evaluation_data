Preheat a lightly oiled griddle over medium heat.    
In a large bowl, sift together flour, sugar, baking powder, baking soda, and salt.    
In a small bowl, beat together egg, orange juice, buttermilk, and canola oil. Whisk into the flour mixture. Stir in pineapple.    
Pour batter about 1/4 cup at a time onto the prepared griddle. Cook 1 to 2 minutes, until bubbly. Flip, and continue cooking until lightly browned.    

