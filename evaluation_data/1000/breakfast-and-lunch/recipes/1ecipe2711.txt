Preheat oven to 300 degrees F (150 degrees C). Line 2 baking sheets with silicone baking mats or parchment paper.    
Combine oats, pumpkin seeds, sunflower seeds, and salt together in a bowl. Add maple syrup and oil; stir until evenly coated. Spread oat mixture onto the prepared baking sheets.    
Bake in the preheated oven for 10 minutes. Remove baking sheets from oven, stir granola, return to oven, and cook until lightly browned, about 7 minutes more. Cool granola on the baking sheets for 20 minutes. Mix cranberries, almonds, and coconut into granola; store in an air-tight container.    

