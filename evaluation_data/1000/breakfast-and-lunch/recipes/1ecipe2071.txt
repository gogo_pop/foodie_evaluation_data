Preheat oven to 375 degrees F (190 degrees C). Coat a 10-inch pie dish with cooking spray.    
Melt butter in a large skillet over medium heat; cook salmon in the hot butter until fish flakes with a fork, about 5 minutes per side. Separate fillets into pieces; stir asparagus, onion, and garlic into skillet with salmon. Cook and stir until liquid clinging to asparagus pieces has evaporated, about 15 minutes. Mix pepper, nutmeg, and 1/2 cup Asiago cheese into salmon mixture. Transfer filling into prepared pie dish.    
Whisk eggs, half-and-half, and salt in a bowl; pour egg mixture over filling in pie dish.    
Bake in the preheated oven for 15 minutes; sprinkle quiche with 1/2 cup Asiago cheese and Cheddar cheese. Continue baking until quiche is set, a knife inserted into the middle comes out clean, and cheese topping is melted, 35 to 40 minutes.    

