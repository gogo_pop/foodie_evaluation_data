In a mixing bowl, mix together olive oil, parsley, lemon juice, hot sauce, garlic, tomato paste, oregano, salt, and black pepper. Reserve a small amount for basting later. Pour remaining marinade into a large resealable plastic bag with shrimp. Seal, and marinate in the refrigerator for 2 hours.    
Preheat grill for medium-low heat. Thread shrimp onto skewers, piercing once near the tail and once near the head. Discard marinade.    
Lightly oil grill grate. Cook shrimp for 5 minutes per side, or until opaque, basting frequently with reserved marinade.    

