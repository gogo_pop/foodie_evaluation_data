In a mixing bowl combine water, egg, and pepper; stir with a fork until well beaten.  Stir in pancake mix; continue stirring until combined.  If the mixture is dry and crumbly add 1/4 cup milk. Mix in the corn, cover and let stand 10 minutes or until firm.    
Heat oil at medium high in a large skillet.  After heating for 5 minutes spoon mixture in 2 tablespoon size lumps into skillet. Do not crowd the skillet. Flip each fritter after the bottom side is golden brown.  Drain on paper towels and repeat until the mixture is gone.    

