Pour the beet juice into a medium-size pot. Stir in the brown sugar, vinegar, water, salt, cloves, and the cinnamon stick. Place the pot over a medium heat for 8 minutes, stirring occasionally.    
Place the beets into the liquid mixture and let it cook for an additional 2 minutes to allow the beets to heat.    
Place the hard cooked eggs (with the shells removed) in a container with a tight-fitting lid.  Pour the liquid and beets into the container with the eggs. Store the container in the refrigerator for approximately 5 days before eating.    

