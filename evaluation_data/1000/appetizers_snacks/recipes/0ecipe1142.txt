In a large bowl, mix chicken, cabbage, carrots, green onions, green beans, green bell pepper and zucchini. In a separate bowl, beat together eggs, flour, chicken stock and soy sauce. Pour batter over chicken mixture and toss to thoroughly coat.    
Mix vegetable oil and sesame oil in a skillet over medium heat. Scoop about 1/4 cup batter into skillet, enough to make a 2 1/2 inch circle. Cover and cook 4 minutes, or until bottom is golden brown. Flip and continue cooking 4 minutes, or until cooked through. Drain on paper towels.    

