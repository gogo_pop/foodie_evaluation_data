Remove skin and bones from fish. In a medium bowl, mix fish with onion and hot pepper sauce with a fork, breaking fish into small pieces. Mix in mayonnaise. Season to taste with salt and pepper. Cover, and refrigerate for 2 hours.    

