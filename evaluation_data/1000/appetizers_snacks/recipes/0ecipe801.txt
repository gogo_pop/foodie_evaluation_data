Preheat oven to 400 degrees F (200 degrees C). Line a baking sheet with aluminum foil and generously spray with cooking spray.    
Mix Parmesan cheese, bread crumbs, garlic powder, onion powder, black pepper, and salt in a bowl.    
Dip chicken wings in melted butter; press into bread crumb mixture until well-coated. Arrange wings on prepared baking dish.    
Bake in preheated oven until golden brown, about 20 minutes. Flip chicken wings and continue baking until evenly browned and no longer pink in the center, about 10 minutes more.    

