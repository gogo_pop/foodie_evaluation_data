In a large bowl, mix the cream cheese, deviled ham, Colby cheese, green olives, and prepared mustard. Season with dry mustard, chopped fresh chives, celery salt, cayenne pepper, and garlic salt. Cover and refrigerate until serving.    

