Preheat oven to 350 degrees F (175 degrees C). Coat a 9 inch pie plate with vegetable oil spray.    
Press cream cheese evenly onto the bottom of the pie plate.    
In a large skillet, brown the hamburger. Drain excess fat. Mix in the taco seasoning and water. Cook and stir 2 to 4 minutes. Remove the skillet from the heat before mixing in salsa and jalapenos. Pour the beef mixture over the cream cheese in the pie plate. Sprinkle the Mexican-style cheese over the entire dish.    
Bake at 350 degrees F (175 degrees C) until the cheese has melted.    

