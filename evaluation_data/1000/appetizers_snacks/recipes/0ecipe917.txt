Preheat the oven to 350 degrees F (175 degrees C). Grease a 9x13 inch baking pan.    
In a large bowl, stir together the brown sugar, peanut butter, corn syrup, applesauce, and vanilla. In a separate bowl, stir together the oats, chocolate chips, mini marshmallows, cereal rings and wheat germ. Stir the dry ingredients into the peanut butter mixture until evenly coated. Press lightly into the prepared pan.    
Bake or 25 to 30 minutes in the preheated oven, or until slightly golden. Cool in the pan on a wire rack. Cut into bars.    

