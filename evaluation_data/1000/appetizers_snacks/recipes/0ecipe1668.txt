Preheat oven to broil. Slice bread into 1/2 inch slices.    
In a medium-size mixing bowl, combine Romano cheese, garlic, and cream cheese. Spread the cheese mixture onto the sliced French bread.    
Broil for about 3 minutes, or until the cheese melts.    

