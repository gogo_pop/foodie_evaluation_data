In a saucepan over medium heat, combine coconut milk, peanut butter, onion, soy sauce, brown sugar, and pepper flakes. Bring to a boil, stirring frequently. Remove from heat, and keep warm.    

