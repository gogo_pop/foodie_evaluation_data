Place eggs in a saucepan and cover with water. Bring to a boil; remove saucepan from heat and let eggs cook in hot water off the heat for 12 minutes. Pour off hot water and place eggs in a bowl full of ice water to cool.    
Peel and halve eggs. Chop about half the egg whites and place into a bowl.    
Put remaining egg whites and yolks in a food processor with mayonnaise, Dijon mustard, white wine vinegar, and hot sauce; process till smooth and transfer to a bowl. Fold chopped egg whites into the mixture to serve.    

