Preheat oven to 450 degrees F (230 degrees C). Lightly grease a baking sheet.    
Combine oil, Parmesan cheese, parsley, oregano, garlic powder, and pepper in a small bowl.    
Cut each biscuit in half; roll each portion into a 6-inch rope and tie in a loose knot. Place on prepared baking sheet.    
Bake in preheated oven until golden brown, 6 to 8 minutes. Immediately brush with Parmesan mixture. Serve warm or freeze up to 2 months.    

