Spread spinach dip evenly over the pizza crust to within 1/2 inch of the edge. Top with broccoli, chicken, green onions and tomato. Cut into wedges to serve.    

