Heat oven to 350 degrees F.    
Combine cheeses and jalapenos in bowl; mix well.    
Place cranberries and sugar in another bowl; toss to coat. Gently stir cranberries into cheese mixture.    
Spread mixture into 1-quart casserole dish. Bake 30-35 minutes or until bubbly around edges.    
Serve with baguette slices, if desired.    

