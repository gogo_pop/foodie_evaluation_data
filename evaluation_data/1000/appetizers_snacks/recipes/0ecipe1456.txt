Preheat oven to 350 degrees F (175 degrees C). Lightly grease a 9x13 inch baking dish.    
In a medium bowl, mix the evaporated milk, water, eggs, Cheddar cheese, flour, baking powder, margarine, green onions and jalapeno peppers.    
Spread mixture evenly into the baking dish. Bake in the preheated oven 30 to 35 minutes, or until lightly browned. Cool, cut into 1 inch squares and serve.    

