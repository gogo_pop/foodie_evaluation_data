Preheat oven to 350 degrees F (175 degrees C).    
Put 1 egg in each of 12 muffin cups.    
Bake in preheated oven for 30 minutes.    
Plunge baked eggs in a large bowl filled with ice water until cooled completely, about 10 minutes.    

