Preheat oven to 450 degrees F (230 degrees C).    
Mix spelt flour, olive oil, salt, 1 teaspoon sugar, with a fork. Starting with 3 tablespoons water, drizzle in just enough for dough to come together. Turn out dough onto work surface. Knead dough until it comes together. Crumble dough and press pieces into the bottoms of 4 small tart pans to form a 1/4-inch-thick bottom crust.    
Crumble 2 ounces goat cheese onto each crust. Lay fig slices on top of goat cheese in a single layer. Season with a pinch of kosher salt, small pinch of cayenne pepper. Sprinkle with 1 tablespoon white sugar.    
Bake until cheese is bubbling and figs are glazed, about 25 minutes. Garnish with sprigs of lemon thyme.    

