Sprinkle gelatin on top of water and set aside.    
Mix the prune pulp, prune juice, sugar, lemon juice, lemon rind, and salt in a saucepan; cook for two minutes.  Add gelatin and water to hot prune mixture, stirring thoroughly, and allow to cool.    
Beat egg whites until stiff, but not dry.  When prune mixture begins to thicken, fold in stiffly beaten egg whites.    
Pour filling into baked pie shell, and chill.  Before serving, spread thin layer of whipped cream or whipped topping over the pie if desired.    

