Preheat oven to 350 degrees F (175 degrees C). Line a 9x13-inch baking pan with parchment paper.    
Mix pumpkin puree, evaporated milk, eggs, granular sweetener, and cinnamon together in a large bowl. Pour into the baking pan.    
Sift dry cake mix over pumpkin mixture. Sprinkle nuts on top; press down lightly. Spoon melted butter evenly over nuts.    
Bake in the preheated oven until a toothpick inserted into the center comes out clean, 50 to 60 minutes. Cool in the pan, 10 to 15 minutes. Invert onto a serving platter and peel off parchment paper.    

