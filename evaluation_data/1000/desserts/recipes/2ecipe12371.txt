Preheat oven to 350 degrees F (175 degrees C). Grease and flour a 9x13-inch baking pan.    
Stir chocolate cake mix, water, vegetable oil, and eggs in a mixing bowl until moistened. Beat with an electric mixer on medium speed for 2 minutes.    
Spread rhubarb over the bottom of the prepared baking dish and sprinkle cherry gelatin mix and sugar evenly over the rhubarb. Pour cake batter over the rhubarb layer.    
Bake in the preheated oven until a toothpick inserted into the middle of the cake layer comes out clean, about 1 hour; check for doneness after 45 minutes. Let cool before serving.    

