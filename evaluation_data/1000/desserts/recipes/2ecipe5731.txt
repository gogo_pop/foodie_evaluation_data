Preheat oven to 375 degrees F(190 degrees C). Grease cookie sheets.    
Melt butter or margarine over low heat.  In a separate bowl, beat together the eggs, sugar, sour cream, and vanilla.  Mix in the melted butter and stir until well blended. Sift together the flour, baking soda, and nutmeg and add to egg mixture, stirring constantly. Drop by teaspoonfuls onto cookie sheets, about 1 inch apart.    
Bake for 10 minutes, or until lightly browned.    

