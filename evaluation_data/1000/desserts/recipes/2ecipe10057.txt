Preheat oven to 350 degrees F (175 degrees C).    
Blend the sugar and the cream. Stir in the flour, almonds and orange peel. Drop by teaspoonfuls onto a heavily creased and floured cookie sheet. Spread mixture into thin circles with a spatula.    
Bake at 350 degrees F (175 degrees C) just until the edges are light brown about 10 to 12 minutes. Let cool a few minutes before removing from the cookie sheet, cool.    
Chop the chocolate up into small pieces. Melt chocolate over low heat or in the microwave on medium for about 3 minutes. Turn the cooled cookies upside down and spread the bottoms with the melted chocolate. Let cookies stand at room temperature until chocolate is firm, at least 3 hours. Store in a covered container at room temperature or refrigerate.    

