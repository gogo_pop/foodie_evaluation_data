Preheat oven to 400 degrees F (200 degrees C). Line 2 baking sheets with parchment paper and lightly grease with coconut oil.    
Stir applesauce and baking soda together in a large bowl until baking soda dissolves. Mix in almond flour, crystallized sugar cane juice, pecans, cranberries, eggs, coconut flour, ground cinnamon, and cake spice. Drop spoonfuls of the dough 2 inches apart onto prepared baking sheets.    
Bake in the preheated oven until set, about 15 minutes.    

