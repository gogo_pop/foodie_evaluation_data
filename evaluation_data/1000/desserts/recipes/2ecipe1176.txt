Preheat oven to 350 degrees F (175 degrees C).    
Place apples in a 9x13 inch baking dish.  In a bowl, cream together butter and sugar.  Stir in flour, instant oatmeal and salt.  Fold in walnuts.  Sprinkle oatmeal mixture evenly over apples.  Drizzle with maple syrup.    
Bake in preheated oven 50 to 70 minutes, until apples are tender and topping is golden.    

