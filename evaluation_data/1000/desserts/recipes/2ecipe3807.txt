Generously butter a 10x15 inch baking pan (with sides).    
In a medium saucepan over medium heat, combine brown sugar, butter, white sugar, water, vinegar and salt.  Cover and bring to a boil.  Remove lid and heat, without stirring, to 270 to 290 degrees F (132 to 143 degrees C), or until a small amount of syrup dropped into cold water forms hard but pliable threads.  Pour in vanilla, but do not stir.  Remove from heat and pour into prepared pan.  Let cool slightly before cutting into squares and allowing candy to cool completely.    

