Preheat oven to 350 degrees F (175 degrees C). Grease cookie sheets.    
In a medium bowl, cream together 1/2 cup butter and white sugar until smooth. Beat in the eggs one at a time, then stir in the vanilla and ricotta cheese. Combine the flour, baking soda and salt; gradually stir into the cheese mixture. Drop by rounded teaspoonfuls 2 inches apart onto the prepared cookie sheets.    
Bake for 8 to 10 minutes in the preheated oven, or until edges are golden. Allow cookies to cool on baking sheet for 5 minutes before removing to a wire rack to cool completely.    
In a medium bowl, cream together the remaining butter and confectioners' sugar. Beat in vanilla and milk gradually until a spreadable consistency is reached. Frost cooled cookies.    

