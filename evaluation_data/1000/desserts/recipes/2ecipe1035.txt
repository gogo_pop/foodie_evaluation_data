Preheat oven to 350 degrees F (175 degrees C). Sift together the 2 1/2 cups flour, baking soda, baking powder and salt. Set aside.    
In small saucepan, melt 6 ounces white chocolate and hot water over low heat. Stir until smooth, and allow to cool to room temperature.    
In a large bowl, cream 1 cup butter and 1 1/2 cup sugar until light and fluffy. Add eggs one at a time, beating well with each addition. Stir in flour mixture alternately with buttermilk. Mix in melted white chocolate.    
Pour batter into two 9 inch round cake pans. Bake for 30 to 35 minutes in the preheated oven, until a toothpick inserted into the center of the cake comes out clean.    
To make Frosting: In a medium bowl, combine 6 ounces white chocolate, 2 1/2 tablespoons flour and 1 cup milk. Cook over medium heat, stirring constantly, until mixture is very thick. Cool completely.    
In large bowl, cream 1 cup butter, 1 cup sugar and 1 teaspoon vanilla; beat until light and fluffy.  Gradually add cooled white chocolate mixture. Beat at high speed until it is the consistency of whipped cream. Spread between layers, on top and sides of cake.    

