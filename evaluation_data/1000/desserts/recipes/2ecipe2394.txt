Prepare and bake cake mix according to package directions for one 9x13 inch cake. Remove cake from oven. Poke holes at once down through cake with a fork.  Holes should be at 1 inch intervals.    
While the cake cools, combine the gelatin with boiling water.  Pour gelatin mixture over the cake.  Top with the cherry pie filling, then cover with whipped topping.  Refrigerate cake for one hour before serving.    

