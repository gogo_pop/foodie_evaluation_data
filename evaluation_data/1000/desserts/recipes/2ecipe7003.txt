Melt chocolate chips in double boiler.  Remove from heat.    
Beat butter and sugar till creamy.  Add egg and syrup,  mix well.  Stir in  melted chocolate.  Add all dry ingredients and mix well.    
Roll dough into 1 1/2 inch balls and roll in sugar.    
Place 3 inches apart on a ungreased cookie sheet.  Bake at 350 degrees F (175 degrees C)  for 12 minutes.  Let cool briefly before removing from pan.    

