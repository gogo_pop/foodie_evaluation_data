Preheat oven to 350 degrees F (175 degrees C).  Grease cookie sheets.    
In a large bowl, cream the shortening and sugar. Add the egg and beat until light and fluffy, then stir in the molasses. In a separate bowl, mix together the flour,  baking soda, salt, ginger and cinnamon.  Add to the egg mixture and stir until well blended. Roll bits of dough into 1 inch balls.  Dip each ball in sugar and place on cookie sheet, sugared side up about 2 inches apart.    
Bake for 10 to 12 minutes, until cookies have spread and tops have cracked.  Let cool on wire rack.    

