Cream together all ingredients except cornflakes and cherries.  Chill dough for one hour.    
Preheat oven to 350 degrees F (180 degrees C).    
Roll into 1 inch  balls, then roll balls  into crumbs.   Place on parchment-lined sheets, top each cookie with a cherry.  Bake for 13 minutes; 6 minutes on middle rack, 7-8 minutes on top rack.    

