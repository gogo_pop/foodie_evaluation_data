This is the repository that hosts the FoodIE evaltuaion dataset. 

As explained in the paper, it consists of two separate sets of 200 and 1000 recipes and extracted foodchunks.

The dataset comprised of 200 recipes is extracted from two separate sites, AllRecipes and MyRecipes (references can be found in the paper), the category being "most popular".

The dataset comprised of 1000 reipces is extracted from AllRecipes, while there are 5 categories of recipes. Namely "Appetizers and snacks", "Breakfast and Lunch", "Desserts", "Dinners" and "Drinks". 